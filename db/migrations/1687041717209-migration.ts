import { MigrationInterface, QueryRunner } from "typeorm";

export class Migration1687041717209 implements MigrationInterface {
    name = 'Migration1687041717209'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "student" ("id" SERIAL NOT NULL, "nombre" character varying NOT NULL, "apellidoPaterno" character varying NOT NULL, "apellidoMaterno" character varying NOT NULL, "email" character varying NOT NULL, "edad" integer NOT NULL, "direccion" character varying NOT NULL, CONSTRAINT "PK_3d8016e1cb58429474a3c041904" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "course" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "codigo" character varying NOT NULL, "anio" integer NOT NULL, "semestre" integer NOT NULL, "sede" character varying NOT NULL, CONSTRAINT "PK_bf95180dd756fd204fb01ce4916" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "course"`);
        await queryRunner.query(`DROP TABLE "student"`);
    }

}
