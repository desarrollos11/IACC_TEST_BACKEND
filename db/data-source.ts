import {DataSource,DataSourceOptions} from 'typeorm';
import { Student } from '../src/students/entities/student.entity';
import { Course } from '../src/courses/entities/course.entity';

export const dataSourceOption: DataSourceOptions = {
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: "app",
    password: "test123",
    database: "iacc_test",
    entities:[Course, Student],
    migrations: ["dist/db/migrations/*.js"]
};
const dataSource = new DataSource(dataSourceOption);
export default dataSource;