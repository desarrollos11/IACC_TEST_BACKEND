import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Course {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  codigo: string;

  @Column()
  anio: number;

  @Column()
  semestre: number;

  @Column()
  sede: string;
}
