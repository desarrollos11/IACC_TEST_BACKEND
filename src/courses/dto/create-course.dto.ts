import { IsNotEmpty, IsString, IsInt } from 'class-validator';

export class CreateCourseDto {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsString()
  codigo: string;


  @IsNotEmpty()
  @IsInt()
  anio: number;


  @IsNotEmpty()
  @IsInt()
  semestre: number;


  @IsNotEmpty()
  @IsString()
  sede: string;

}