import { IsOptional, IsString,IsInt } from 'class-validator';

export class UpdateCourseDto {
  @IsString()
  name: string;

  @IsString()
  codigo: string;

  @IsInt()
  anio: number;
  
  @IsInt()
  semestre: number;
  @IsString()
  sede: string;
}
