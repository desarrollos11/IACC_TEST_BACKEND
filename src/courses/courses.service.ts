import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Course } from './entities/course.entity';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';

@Injectable()
export class CoursesService {
  constructor(
    @InjectRepository(Course)
    private readonly courseRepository: Repository<Course>,
  ) {}

  async getAllCourses() {
    return this.courseRepository.find();
  }

  async getCourseById(id: number) {
    return this.courseRepository.findOne({ where: { id } });
  }

  async createCourse(createCourseDto: CreateCourseDto) {
    const course = this.courseRepository.create(createCourseDto);
    return this.courseRepository.save(course);
  }

  async updateCourse(id: number, updateCourseDto: UpdateCourseDto) {
    const course = await this.getCourseById(id);
    if (!course) {
      throw new NotFoundException('Course not found');
    }
    Object.assign(course, updateCourseDto);
    return this.courseRepository.save(course);
  }

  async deleteCourse(id: number) {
    const course = await this.getCourseById(id);
    if (!course) {
      throw new NotFoundException('Course not found');
    }
    return this.courseRepository.remove(course);
  }
}
