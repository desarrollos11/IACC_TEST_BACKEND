import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Student } from './entities/student.entity';
import { CreateStudentDto } from './dto/create-student.dto';
import { UpdateStudentDto } from './dto/update-student.dto';

@Injectable()
export class StudentsService {
  constructor(
    @InjectRepository(Student)
    private readonly studentRepository: Repository<Student>,
  ) {}

  async getAllStudents() {
    return this.studentRepository.find();
  }

  async getStudentById(id: number) {
    return this.studentRepository.findOne({ where: { id } });
  }

  async createStudent(createStudentDto: CreateStudentDto) {
    const student = this.studentRepository.create(createStudentDto);
    return this.studentRepository.save(student);
  }

  async updateStudent(id: number, updateStudentDto: UpdateStudentDto) {
    const student = await this.getStudentById(id);
    if (!student) {
      throw new NotFoundException('Student not found');
    }
    Object.assign(student, updateStudentDto);
    return this.studentRepository.save(student);
  }

  async deleteStudent(id: number) {
    const student = await this.getStudentById(id);
    if (!student) {
      throw new NotFoundException('Student not found');
    }
    return this.studentRepository.remove(student);
  }
}
