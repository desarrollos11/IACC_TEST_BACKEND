import { IsNotEmpty, IsString, IsInt, Min } from 'class-validator';

export class CreateStudentDto {
  @IsNotEmpty()
  @IsString()
  nombre: string;

  @IsNotEmpty()
  @IsString()
  apellidoPaterno: string;

  @IsNotEmpty()
  @IsString()
  apellidoMaterno: string;

  @IsNotEmpty()
  @IsString()
  email: string;

  
  @IsNotEmpty()
  @IsInt()
  @Min(1)
  edad: number;
  
  @IsNotEmpty()
  @IsString()
  direccion: string;

}


