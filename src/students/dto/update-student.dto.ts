import { IsOptional, IsString, IsInt, Min } from 'class-validator';

export class UpdateStudentDto {
    @IsString()
    nombre: string;
  
    @IsString()
    apellidoPaterno: string;
  
    @IsString()
    apellidoMaterno: string;
  
    @IsString()
    email: string;
  
    
    @IsInt()
    @Min(1)
    edad: number;
    
    @IsString()
    direccion: string;
}
