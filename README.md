# IACC TEST BACKEND

## Para iniciar el proyecto
-Crear una BD en su localhost con el nombre "iacc_test"
- configurar sus credenciales de usuario de la BD en ./db/data-source.ts

para la creacion de tablas en su BD:
Dentro de una linea de comandos, posicionese en el directorio del proyecto y ejecute el comando:
- npm install
- npm migration:generate
- migration:run

se crearan las tablas en su BD.

finalmente npm start, ejecutara la aplicacion en su localhost puerto 3000.

## Para la prueba de estres RUTA DEL ARCHIVO EN RAIZ DEL PROYECTO: .asciiart-load-test.yml

artillery run asciiart-load-test.yml


Ejemplos de comandos CURL para consumir la API

Obtener todos los cursos:
curl -X GET http://localhost:3000/courses

Obtener un curso por ID:

curl -X GET http://localhost:3000/courses/{id}

Crear un nuevo curso:

curl -X POST -H "Content-Type: application/json" -d '{"name":"Updated Student Name","codigo":"cod123", "anio":2023, "semestre":2,"sede":"santiago"}' http://localhost:3000/courses

Actualizar un curso existente:

curl -X PUT -H "Content-Type: application/json" -d '{"name":"Updated Student Name","codigo":"cod123", "anio":2023, "semestre":2,"sede":"santiago"}' http://localhost:3000/courses/{id}

Eliminar un curso:

curl -X DELETE http://localhost:3000/courses/{id}

Obtener todos los estudiantes:

curl -X GET http://localhost:3000/students

Obtener un estudiante por ID:

curl -X GET http://localhost:3000/students/{id}

Crear un nuevo estudiante:

curl -X POST -H "Content-Type: application/json" -d '{"nombre":"Juan","apellidoPaterno":"Perez", "apellidoMaterno":"Lopez", "email":"email@email.cl","edad":19,"direccion":"calle123"}' http://localhost:3000/students

Actualizar un estudiante existente:

curl -X PUT -H "Content-Type: application/json" -d '{"nombre":"Juan","apellidoPaterno":"Perez", "apellidoMaterno":"Lopez", "email":"email@email.cl","edad":19,"direccion":"calle123"}' http://localhost:3000/students/{id}

Eliminar un estudiante:

curl -X DELETE http://localhost:3000/students/{id}